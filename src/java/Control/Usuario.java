    
package Control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
@SessionScoped
public class Usuario {

    int id;
    private String name,lastname,address,phone,password,email;
    private int tusu,sector;
    private String valEmail, valPassword;
    Connection connection;
    String sql_String;
    ArrayList usersList;
    private Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

    public Connection getConnection(){
        
        try{
            
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/reusable","root","");
        }catch(Exception e){
            System.out.println(e);
        }
        
        return connection;
    }
    
    
    public String saveData(){
        
        int result = 0;
        try{
            connection = getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO `usuarios`(`Nombres`, `Apellidos`, `correo`, `clave`, `Direccion`, "
                                                             + "`TelefonoMovil`, `Tipo_id`, `Sector_id`, `Activo`) VALUES (?,?,?,?,?,?,?,?,'SI')");
            stmt.setString(1,name);
            stmt.setString(2,lastname);
            stmt.setString(3,email);
            stmt.setString(4,password);
            stmt.setString(5,address);
            stmt.setString(6,phone);
            stmt.setInt(7,tusu);
            stmt.setInt(8,sector);
            result = stmt.executeUpdate();
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        if(result != 0)
            return "index.xhtml?faces-redirect=true";
        else return "inicioS.xhtml?faces-redirect=true";
    }
    
    public ArrayList usersList(int ids){
        try{
            usersList = new ArrayList();
            connection = getConnection();
            Statement stmt=getConnection().createStatement();
            ResultSet rs=stmt.executeQuery("select * from usuarios where id = '"+ ids +" ' ");
            while(rs.next()){
                Usuario user = new Usuario();
                user.setName(rs.getString("name"));
                user.setLastname(rs.getString("lastname"));
                user.setEmail(rs.getString("email"));
                user.setAddress(rs.getString("address"));
                usersList.add(user);
            }
            connection.close();
        }catch(Exception e){
            System.out.println(e);
        }
        
        return usersList;
    }
    
    public void dbSearch(String correO){
        try {
            connection = getConnection();
            Statement stmt = connection.createStatement();
            sql_String = "SELECT * FROM `usuarios` WHERE `correo` LIKE ('"+ correO +"')";
            ResultSet rs = stmt.executeQuery(sql_String);
            rs.next();
            valEmail = rs.getString(3).toString();
            valPassword = rs.getString(4).toString();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
    
    public String validateLogin(){
        dbSearch(email);
        if(email.equalsIgnoreCase(valEmail)){
            if(password.equals(valPassword)){
                return "valid";
            }else{
                return "invalid";
            }
        }else{
            return "invalid";
        }
    }
    
    
    public String getValEmail() {
        return valEmail;
    }

    public void setValEmail(String valEmail) {
        this.valEmail = valEmail;
    }

    public String getValPassword() {
        return valPassword;
    }

    public void setValPassword(String valPassword) {
        this.valPassword = valPassword;
    }
    
    public Usuario() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTusu() {
        return tusu;
    }

    public void setTusu(int tusu) {
        this.tusu = tusu;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }
    
}
